const app = require('express')();
import http from 'http';
const server = http.createServer(app);
const io = require('socket.io')(server);
import { EventEmitter } from 'events';
import { delay } from './util';

const max_players = 2;
const shots_per_turn = 3;
const num_rounds = 1;
const admin_cookie = "admin";

app.set('port', (process.env.PORT || 3001));
console.log('The port is:::: ', app.get('port'));

server.listen(app.get('port'), () => {
  console.log('---> listening on port ', app.get('port'));
});

io.on('connection', (socket: SocketIO.Socket) => {
  socket.on('disconnect', () => {

  });

  emit_game_state();

  socket.on('turn', (data: any) => {
    if (game_events['turn']) game_events['turn'](data);
  });


  socket.on('score_input', (data: any) => {
    if (game_events['score_input']) game_events['score_input'](data);
  });


  socket.on('join', (data: { name: string }) => {
    if (game_events['player_join']) game_events['player_join']({
      socket,
      name: data.name,
    });
  });

  socket.on('test_cookie', (data: Cookie) => {
    try {
      if (game_state.players[data.player].cookie === data.cookie)
        return;
      socket.emit('clear_cookie');
    } catch (err) {
      socket.emit('clear_cookie');
    }
  });

  socket.on('game_reset', (data: any) => {
    try {
      if (data.admin_cookie === admin_cookie) game_reset();
    } catch (err) { }
  })
});


const bridge = io.of('/bridge');
bridge.on('connection', (socket: SocketIO.Socket) => {
  console.log('rpi bridge connected');

  socket.on('ready', () => {
    io.emit('ready');
  });

  socket.on('disconnect', () => {
    console.error('rpi bridge disconnected');
  });
});



interface InternalGameState {
  players: Player[];
  stage: Player_Select | Turn | Pause | Game_Over;
}

interface PublicGameState {
  players: Player_Public[];
  max_players: number;
  stage: Player_Select | Turn | Pause | Game_Over;
}

interface Player_Select {
  name: "player_select";
  players: number;
  max_players: number;
};

interface Pause {
  name: "pause";
};
interface Turn {
  name: "turn";
  player: number;
  shot: number;
  shots: number;
  round: number;
  rounds: number;
  x?: number;
  y?: number;
}

interface Game_Over {
  name: "game_over"
}


interface Player {
  cookie: string;
  name: string;
  score: number;
  score_add?: number;
}

interface Player_Public {
  name: string;
  score: number;
  score_add?: number;
}



const emit_game_state = () => {
  const pst: PublicGameState = {
    players: game_state.players.map(p => {
      return {
        name: p.name,
        score: p.score,
	score_add: p.score_add,
      }
    }),
    stage: game_state.stage,
    max_players: max_players,
  }
  console.log('emitting state', game_state);
  io.emit('game_state', pst);
}

const game_state: InternalGameState = {
  players: [],
  stage: {
    name: "player_select",
    players: 0,
    max_players: max_players,
  },
};

const game_loop = async () => {
  game_events = {};
  game_state.players = []
  game_state.stage = {
    name: "player_select",
    players: 0,
    max_players: max_players,
  }

  const select_stage = game_state.stage as Player_Select;
  console.log("waiting for players");
  for (; select_stage.players < max_players; select_stage.players++) {
    emit_game_state();
    game_state.players[select_stage.players] = await player_join(select_stage.players);
  }

  for (let r = 0; r < num_rounds; r++) {
    for (let n = 0; n < game_state.players.length; n++) {
      for (let k = 0; k < shots_per_turn; k++) {
        console.log("turn");

        game_state.stage = {
          name: "turn",
          player: n,
          shot: k,
          shots: shots_per_turn,
          round: r,
          rounds: num_rounds,
        }
        emit_game_state();
        const t = await turn(game_state.players[n]);
        console.log("turn", t);

        const st = game_state.stage as Turn;
        st.x = t.x;
        st.y = t.y;
        emit_game_state();

        console.log('fire');
        bridge.emit('fire', {
          x: Math.round(t.x),
          y: Math.round(t.y),
          duration: 1000,
        });

        const score = await score_input();
	game_state.players[n].score_add = score.score;
	emit_game_state();
	await game_delay(1500);
        game_state.players[n].score += score.score;
	delete game_state.players[n].score_add;
      }
    }
  }

  game_state.stage = {
    name: "game_over"
  }
  emit_game_state();
  console.log("GAME END");

  await game_delay(40000);
  io.emit('clear_cookie');
  game_loop();
}

interface TurnCommand {
  x: number;
  y: number;
  cookie: Cookie;
};

interface Score_Input {
  score: number;
  admin_cookie: string;
}

let game_events: { [key: string]: (data?: any) => void } = {};

const game_delay = async (time: number) => {
  return new Promise(async (res, rej) => {
    const x = "delay" + Math.random().toString();
    game_events[x] = async () => {
      delete game_events[x];
      res();
    }
    await delay(time);
    if (game_events[x]) game_events[x]();
  });
}

const score_input = async (): Promise<Score_Input> => {
  return new Promise((res, rej) => {
    game_events['score_input'] = async (score: Score_Input) => {
      if (score.admin_cookie === admin_cookie) {
        delete game_events['score_input']
        res(score);
      } else console.log('unauthorized_command');
    }
  });
}

const turn = async (p: Player): Promise<TurnCommand> => {
  return new Promise((res, rej) => {
    game_events['turn'] = async (td: TurnCommand) => {
      if (td.cookie && td.cookie.cookie === p.cookie) {
        delete game_events['turn']
        res(td);
      } else console.log('unauthorized_command');
    }
  });
}



const player_join = async (n: number): Promise<Player> => {
  return new Promise((res, rej) => {
    game_events['player_join'] = (data: { socket: SocketIO.Socket, name: string }) => {
      delete game_events['player_join'];
      const player: Player = {
        cookie: Math.random().toString(36).substring(7),
        name: data.name,
        score: 0,
      };
      data.socket.emit('cookie', { cookie: player.cookie, player: n });
      res(player);
    };
  });
}


const game_reset = () => {
  io.emit('clear_cookie');
  game_loop();
}

game_loop();


interface Cookie {
  cookie: string;
  player: number;
}
